#!/bin/bash
SERVICE_NAME=zipkin-distributed-tracing-server
cd $(cd -P -- "$(dirname -- "$0")" && pwd -P)

#mvn clean install -DskipTests
docker build -t $SERVICE_NAME .

HOST_IP=$1
docker run --net="host" -d -p 9411:9411 -e SERVICE_PORT=9411 -e HOST_IP=$HOST_IP -e SERVICE_NAME=$SERVICE_NAME --hostname localhost $SERVICE_NAME
