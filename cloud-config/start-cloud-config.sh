#!/bin/bash
function cdToWorkDir {
               cd $(cd -P -- "$(dirname -- "$0")" && pwd -P)
           }
cdToWorkDir

for f in *; do
    if [[ -d $f ]]; then
        $f/docker-start.sh 127.0.0.1 $1 $2 $3
    fi
done
docker run -d -p 5672:5672 rabbitmq:3
