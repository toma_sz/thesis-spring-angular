#!/bin/bash
SERVICE_NAME=zuul-gateway
cd $(cd -P -- "$(dirname -- "$0")" && pwd -P)

#mvn clean install -DskipTests
docker build -t $SERVICE_NAME .

HOST_IP=$1
docker run --net="host" -d -p 8765:8765 -e SERVICE_PORT=8765 -e HOST_IP=$HOST_IP -e SERVICE_NAME=$SERVICE_NAME --hostname localhost $SERVICE_NAME
