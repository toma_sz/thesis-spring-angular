#!/bin/bash
SERVICE_PORT=8765
CLOUD_SERVER_PORT='8888'
EUREKA_PATH='8761/eureka'
JAR_NAME=$SERVICE_NAME*.jar

SPRING_OPTS="-Deureka.client.serviceUrl.defaultZone=http://$HOST_IP:$EUREKA_PATH -Dspring.rabbitmq.host=$HOST_IP -Dspring.profiles.active=DOCKER"

/usr/lib/jvm/java-8-openjdk-amd64/bin/java $JAVA_OPTS -Dserver.port=$SERVICE_PORT -jar $SPRING_OPTS  /root/$JAR_NAME