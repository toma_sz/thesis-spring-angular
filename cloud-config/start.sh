#!/bin/bash
EUREKA_PATH='8761/eureka'
SPRING_OPTS="-Dspring.cloud.config.uri=http://$HOST_IP:$CLOUD_SERVER_PORT -Deureka.client.serviceUrl.defaultZone=http://$HOST_IP:$EUREKA_PATH -Dspring.rabbitmq.host=$HOST_IP -Dspring.profiles.active=DOCKER"
JAVA_OPTS="-Xmx512m"
nohup /usr/lib/jvm/java-8-openjdk-amd64/bin/java $JAVA_OPTS $SPRING_OPTS -jar /root/cloud-config-server.jar > /root/cloud-config-server.out &
sleep 5

#JAVA_OPTS=-Xms 64M -Xmx 64M -Xss 64M

nohup /usr/lib/jvm/java-8-openjdk-amd64/bin/java $JAVA_OPTS  $SPRING_OPTS -jar /root/eureka-naming.jar > /root/eureka-naming.out &
nohup /usr/lib/jvm/java-8-openjdk-amd64/bin/java $JAVA_OPTS  $SPRING_OPTS -jar /root/zuul-gateway.jar > /root/zuul-gateway.out &
nohup /usr/lib/jvm/java-8-openjdk-amd64/bin/java $JAVA_OPTS  $SPRING_OPTS -jar /root/zipkin-distributed-tracing-server.jar > /root/zipkin-distributed-tracing-server.out &

tail -f /dev/null

