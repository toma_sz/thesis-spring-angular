#!/bin/bash
SERVICE_NAME=cloud-config-server
cd $(cd -P -- "$(dirname -- "$0")" && pwd -P)

#mvn clean install -DskipTests
docker build -t $SERVICE_NAME .

HOST_IP=$1
docker run --net="host" -d -p 8888:8888 -e SERVICE_PORT=8888 -e HOST_IP=$HOST_IP -e SERVICE_NAME=$SERVICE_NAME -e GIT_USERNAME=$2 -e GIT_PASSWORD=$3 -e GIT_URL=$4 --hostname localhost $SERVICE_NAME
echo "docker run --net="host" -d -p 8888:8888 -e SERVICE_PORT=8888 -e HOST_IP=$HOST_IP -e SERVICE_NAME=$SERVICE_NAME -e GIT_USERNAME=$2 -e GIT_PASSWORD=$3 -e GIT_URL=$4 --hostname localhost $SERVICE_NAME"