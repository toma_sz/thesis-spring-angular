#!/bin/bash
SERVICE_PORT=8888
CLOUD_SERVER_PORT='8888'
EUREKA_PATH='8761/eureka'
JAR_NAME=$SERVICE_NAME*.jar

SPRING_OPTS=" -Dspring.rabbitmq.host=$HOST_IP -Dspring.profiles.active=DOCKER -Dspring.cloud.config.server.git.uri=$GIT_URL -Dspring.cloud.config.server.git.username=$GIT_USERNAME -Dspring.cloud.config.server.git.password=$GIT_PASSWORD"

/usr/lib/jvm/java-8-openjdk-amd64/bin/java $JAVA_OPTS -Dserver.port=$SERVICE_PORT -jar $SPRING_OPTS  /root/$JAR_NAME