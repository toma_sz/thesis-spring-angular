#!/bin/bash
function cdToWorkDir {
               cd $(cd -P -- "$(dirname -- "$0")" && pwd -P)
           }
     
if [ "$#" -ne 3 ]; then
    echo "Program requeires 3 arguments. Usage - GIT username, password, URL."
    exit 1
fi
cdToWorkDir
docker pull tutum/mysql:5.5
cp schema.sql /tmp
docker run -d -p 3306:3306 -v /tmp:/tmp  -e STARTUP_SQL="/tmp/schema.sql" -e MYSQL_PASS="admin" tutum/mysql:5.5
cdToWorkDir
./cloud-config/start-cloud-config.sh $1 $2 $3
cdToWorkDir
./services/start-services.sh 12000

cd frontend
#ng build
docker build -t angular-gui .
docker run -d \
  -p 4200:80 \
  angular-gui
