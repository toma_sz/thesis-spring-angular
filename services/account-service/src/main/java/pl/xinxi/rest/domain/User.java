package pl.xinxi.rest.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long userId;

    @NotEmpty
    @Column(unique = true)
    private String username;

    private String hashedPassword;

    private String salt;

    @Email
    @Column(unique = true)
    private String email;

    private boolean isActivated;

    private String guid;

    private Date userCreated;

    @Enumerated(EnumType.STRING)
    private Role role;

    public User(String username, String hashedPassword, String salt, String email, boolean isActivated, String guid, Date userCreated) {
        this.username = username;
        this.hashedPassword = hashedPassword;
        this.salt = salt;
        this.email = email;
        this.isActivated = isActivated;
        this.guid = guid;
        this.userCreated = userCreated;
    }



}
