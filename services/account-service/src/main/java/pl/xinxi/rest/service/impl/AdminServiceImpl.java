package pl.xinxi.rest.service.impl;

import org.springframework.stereotype.Service;
import pl.xinxi.rest.domain.Role;
import pl.xinxi.rest.domain.User;
import pl.xinxi.rest.repository.UserRepository;
import pl.xinxi.rest.service.AdminService;

@Service
public class AdminServiceImpl implements AdminService{
    private UserRepository userRepository;

    public AdminServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void promoteToAdmin(Long userId) {
        User user = userRepository.getByUserId(userId).orElseThrow(IllegalArgumentException::new);
        user.setRole(Role.ADMIN);
        userRepository.save(user);
    }
}
