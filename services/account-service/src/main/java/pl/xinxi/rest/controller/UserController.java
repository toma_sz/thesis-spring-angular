package pl.xinxi.rest.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.xinxi.rest.domain.ApiKey;
import pl.xinxi.rest.domain.HttpResponse;
import pl.xinxi.rest.domain.User;
import pl.xinxi.rest.domain.UserWeb;
import pl.xinxi.rest.service.UserService;

import javax.servlet.ServletException;
import java.util.Map;

@RestController
@CrossOrigin
public class UserController {
   UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/allUsers")
    public Iterable<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @PostMapping("/addUser")
    public ResponseEntity addUser(
            @RequestBody UserWeb user
    ) {
        userService.addUser(user.getUsername(),user.getEmail(),user.getPassword());

        return ResponseEntity.ok(new HttpResponse("OK"));
    }

    @PatchMapping("/patchUser/{id}")
    public ResponseEntity editUser(@RequestBody Map<String, Object> updates, @PathVariable("id") int id){
        if(userService.patchUser(updates, id)) return ResponseEntity.ok(new HttpResponse("OK"));
        return ResponseEntity.status(404).body(new HttpResponse("NOT_FOUND"));
    }

    @DeleteMapping("/deleteUser")
    public ResponseEntity deleteUser(@RequestParam("username")String username){
    if(userService.deleteUserByUsername(username)) return ResponseEntity.ok().body(new HttpResponse("OK"));
    return ResponseEntity.status(404).body(new HttpResponse("NOT_FOUND"));
    }

    @GetMapping("/activateUser/{guid}")
    public ResponseEntity activateUser(@PathVariable("guid")String guid){
        if (userService.activateUser(guid)) return ResponseEntity.ok(new HttpResponse("OK"));
        return ResponseEntity.status(404).body(new HttpResponse("NOT_FOUND"));
    }

    @PostMapping(value = "/login")
    public ResponseEntity login(@RequestBody UserWeb user) throws ServletException {
        return ResponseEntity.ok(new ApiKey(userService.login(user.getUsername(),user.getPassword())));
    }
}
