package pl.xinxi.rest.domain;

public enum Role {
    USER,
    ADMIN
}
