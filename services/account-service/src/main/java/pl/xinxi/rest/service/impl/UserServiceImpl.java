package pl.xinxi.rest.service.impl;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Service;
import pl.xinxi.rest.domain.Role;
import pl.xinxi.rest.domain.User;
import pl.xinxi.rest.repository.UserRepository;
import pl.xinxi.rest.service.UserService;
import pl.xinxi.rest.service.feignclients.EmailServiceProxy;

import javax.transaction.Transactional;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private EmailServiceProxy emailServiceProxy;

    public UserServiceImpl(UserRepository userRepository,EmailServiceProxy emailServiceProxy) {
        this.userRepository = userRepository;
        this.emailServiceProxy = emailServiceProxy;
    }

    @Override
    public Optional<User> getByUsername(String username) {
        return userRepository.getByUsername(username);
    }

    @Override
    public Optional<User> getUser(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public void addUser(String username, String email, String password) {
        String guid = UUID.randomUUID().toString();
        String salt = UUID.randomUUID().toString();
        Date date = new Date();

        String saltedPasswordHash = get_SHA_512_SecurePassword(password,salt);

        userRepository.save(new User(username,saltedPasswordHash,salt,email,false, guid, date));

        User user = userRepository.getByUsername(username).get();

        user.setRole(Role.USER);

        emailServiceProxy.sendActivationUrl(user);
        userRepository.save(user);
    }

    @Override
    public boolean deleteUserByUsername(String username) {
        int count = userRepository.deleteByUsername(username);
        return  count==0 ? false : true;
    }

    @Override
    public boolean patchUser(Map<String, Object> updates, long id) {
        Optional<User> userOptional = userRepository.getByUserId(id);
        if (!userOptional.isPresent()) return false;
        User user = userOptional.get();
        for(String key : updates.keySet()){
            if (key.equals("username")) user.setUsername(updates.get(key).toString());
            if (key.equals("hashedPassword")) user.setHashedPassword(updates.get(key).toString());
            if (key.equals("email")) user.setEmail(updates.get(key).toString());
        }
        userRepository.save(user);
        return true;
    }

    @Override
    public Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }


    @Override
    public boolean activateUser(String guid) {
        Optional<User> userOptional = userRepository.getByGuid(guid);
        if (!userOptional.isPresent()) return false;
        User user = userOptional.get();
        user.setActivated(true);
        userRepository.save(user);
        return true;
    }

    @Override
    public String login(String username, String password) {
        if (username == null || password == null) {
            throw new RuntimeException("Please fill in username and password");
        }
        Optional<User> userOptional = userRepository.getByUsername(username);
        if (!userOptional.isPresent()) throw new RuntimeException("Login not exists");

        User user = userOptional.get();
        String databasePassword = user.getHashedPassword();
        String passwordFromURL = get_SHA_512_SecurePassword(password,user.getSalt());
        if (!passwordFromURL.equals(databasePassword)) {
            throw new RuntimeException("Password or login not correct");
        }
        if (!user.isActivated()) throw new RuntimeException("User not activated");
        return Jwts.builder()
                .setSubject(username).
                        claim("role", user.getRole()).
                        claim("userId", user.getUserId()).
                        setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS512, "secretkey")
                .setExpiration(new Date( System.currentTimeMillis() + 5000000L)).compact();
    }

    @Override
    public void changePassword(String username, String password) {
        User user = userRepository.getByUsername(username).orElseThrow(IllegalArgumentException::new);
        user.setHashedPassword(get_SHA_512_SecurePassword(password, user.getSalt()));
        userRepository.save(user);
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    private  String get_SHA_512_SecurePassword(String passwordToHash, String salt){
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(salt.getBytes("UTF-8"));
            byte[] bytes = md.digest(passwordToHash.getBytes("UTF-8"));
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++){
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException | UnsupportedEncodingException e ){
            e.printStackTrace();
        }
        return generatedPassword;
    }
}
