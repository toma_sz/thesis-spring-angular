package pl.xinxi.rest;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import pl.xinxi.rest.domain.Role;
import pl.xinxi.rest.domain.User;
import pl.xinxi.rest.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.Date;

//@Component
public class Bootstrap implements ApplicationListener<ContextRefreshedEvent> {

    UserRepository userRepository;

    public Bootstrap(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {
      User user = new User("admin", "", "", "", true, "", new Date());
      user.setRole(Role.ADMIN);
      userRepository.save(user);
    }
}
