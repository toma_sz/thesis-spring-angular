package pl.xinxi.rest.service.feignclients;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import pl.xinxi.rest.domain.User;

@FeignClient(name = "zuul-gateway")
@RibbonClient(name = "email-service")
public interface EmailServiceProxy {

    @PostMapping("email-service/activateUser")
    ResponseEntity sendActivationUrl(@RequestBody User user);
}
