package pl.xinxi.rest.service;

import pl.xinxi.rest.domain.User;

import java.util.Map;
import java.util.Optional;

public interface UserService {

    void addUser(String username, String email, String password);
    Iterable<User> getAllUsers();
    Optional<User> getByUsername(String username);
    Optional<User> getUser(Long id);
    boolean deleteUserByUsername(String username);
    boolean patchUser(Map<String, Object> updates, long id);
    boolean activateUser(String guid);
    String login(String username, String password);
    void changePassword(String username, String password);
    void save(User user);
}
