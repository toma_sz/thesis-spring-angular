package pl.xinxi.rest.controller;

import io.jsonwebtoken.Claims;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.xinxi.rest.domain.HttpResponse;
import pl.xinxi.rest.domain.UserCredentials;
import pl.xinxi.rest.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/userDetails")
@CrossOrigin
public class UserDetails {
    private UserService userService;

    public UserDetails(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/changePassword")
    public ResponseEntity saltByUsername(HttpServletRequest request,@RequestParam("newPassword") String newPassword){
        Claims claims = (Claims) request.getAttribute("claims");
        return ResponseEntity.ok(new HttpResponse("OK"));
    }

    @GetMapping("/authenticateUser")
    public ResponseEntity<UserCredentials> authenticateUserJWT(HttpServletRequest request){
        Claims claims = (Claims) request.getAttribute("claims");
        System.out.println(new UserCredentials(Long.valueOf((Integer) claims.get("userId")),claims.getSubject(),claims.get("role").toString()));
        return ResponseEntity.ok(new UserCredentials(Long.valueOf((Integer) claims.get("userId")), claims.getSubject(),claims.get("role").toString()));
    }

    @GetMapping("/getUsername")
    public ResponseEntity<Map<String, String>> getUsername(HttpServletRequest request){
        Claims claims = (Claims) request.getAttribute("claims");
        Map<String,String> map = new HashMap<>();
        map.put("username",userService.getUser(Long.valueOf((Integer) claims.get("userId"))).get().getUsername());
        return ResponseEntity.ok(map);
    }
}
