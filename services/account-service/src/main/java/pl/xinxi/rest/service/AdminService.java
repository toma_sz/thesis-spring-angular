package pl.xinxi.rest.service;

public interface AdminService {
    void promoteToAdmin(Long userId);
}
