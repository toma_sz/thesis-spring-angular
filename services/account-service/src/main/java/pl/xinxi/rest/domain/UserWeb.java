package pl.xinxi.rest.domain;

import lombok.Data;

@Data
public class UserWeb {
    private String username;
    private String password;
    private String email;
}
