package pl.xinxi.rest.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.xinxi.rest.service.AdminService;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/admin")
@CrossOrigin
public class AdminController {
    private AdminService adminService;

    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    @GetMapping("/promoteToAdmin")
    public ResponseEntity saltByUsername(HttpServletRequest request,@RequestParam("userId") Long userId){
        adminService.promoteToAdmin(userId);
        return ResponseEntity.ok("STATUS.OK");
    }


}
