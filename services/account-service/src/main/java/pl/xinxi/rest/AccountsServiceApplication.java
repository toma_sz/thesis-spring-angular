package pl.xinxi.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import pl.xinxi.rest.conf.JwtAdminFilter;
import pl.xinxi.rest.conf.JwtFilter;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients("pl.xinxi.rest.service.feignclients")
public class AccountsServiceApplication {

	@Bean
	public FilterRegistrationBean jwtFilter() {
		final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		registrationBean.setFilter(new JwtFilter());
		registrationBean.addUrlPatterns("/userDetails/*");

		return registrationBean;
	}

	@Bean
	public FilterRegistrationBean jwtAdminFilter() {
		final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		registrationBean.setFilter(new JwtAdminFilter());
		registrationBean.addUrlPatterns("/admin/*");

		return registrationBean;
	}

	public static void main(String[] args) {
		SpringApplication.run(AccountsServiceApplication.class, args);
	}
}
