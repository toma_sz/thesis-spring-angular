package pl.xinxi.rest.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.xinxi.rest.domain.User;

import java.util.Optional;


@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    Optional<User> getByUsername(String username);
    Optional<User> getByUserId(long id);
    Optional<User> getByGuid(String guid);
    int deleteByUsername(String username);
}

