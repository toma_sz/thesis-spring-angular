package pl.xinxi.rest.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
public class UserCredentials {
    private Long id;
    private String username;
    private String role;

}
