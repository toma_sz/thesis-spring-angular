#!/bin/bash
SERVICE_NAME=accounts-service
cd $(cd -P -- "$(dirname -- "$0")" && pwd -P)

#mvn clean install -DskipTests
docker build -t $SERVICE_NAME .
if [ "$#" -ne 2 ]; then
    echo "Illegal number of parameters"
    echo "Usage PORT host_ip"
    exit
fi

PORT=$1
HOST_IP=$2
docker run --net="host" -d -p $PORT:$PORT -e SERVICE_PORT=$PORT -e HOST_IP=$HOST_IP -e SERVICE_NAME=$SERVICE_NAME --hostname localhost $SERVICE_NAME

