#!/bin/bash

CLOUD_SERVER_PORT='8888'
EUREKA_PATH='8761/eureka'
JAR_NAME=$SERVICE_NAME*.jar

SPRING_OPTS="-Dspring.cloud.config.uri=http://$HOST_IP:$CLOUD_SERVER_PORT -Deureka.client.serviceUrl.defaultZone=http://$HOST_IP:$EUREKA_PATH -Dspring.rabbitmq.host=$HOST_IP -Dspring.profiles.active=DOCKER -Dspring.application.name=$SERVICE_NAME"

/usr/lib/jvm/java-8-openjdk-amd64/bin/java $JAVA_OPTS -Dserver.port=$SERVICE_PORT -jar $SPRING_OPTS  /root/$JAR_NAME