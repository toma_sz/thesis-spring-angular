package pl.xinxi.rest.emailservice.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

@Component
public class EmailSender {
    private Properties mailServerProperties;


    @Value("${url.activation}")
    private String activationUrl;

    @Value("${email.subject}")
    private String emailSubject;

    @Value("${email.body}")
    private String emailBody;
    @Autowired
    public JavaMailSender emailSender;

    public void sendEmail(User user){
        new Thread(new Emailer(user)).start();
    }
    private class Emailer implements Runnable{
        User user;
        public Emailer(User user) {
            this.user = user;
        }

        @Override
        public void run() {
            SimpleMailMessage message = new SimpleMailMessage();
            String ip = "localhost";
            try {
                ip = InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
            message.setTo(user.getEmail());
            message.setSubject(emailSubject);
            message.setText("Hello " + user.getUsername() + ",\n" + emailBody + "\n" + activationUrl.replace("%ip%",ip ) + user.getGuid());
            emailSender.send(message);
        }
    }
}
