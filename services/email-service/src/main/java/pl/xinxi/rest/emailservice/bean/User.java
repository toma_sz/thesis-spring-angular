package pl.xinxi.rest.emailservice.bean;

import lombok.*;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by SG0301080 on 10/8/2017.
 */
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long userId;

    @NotEmpty
    @Column(unique = true)
    private String username;

    private String hashedPassword;

    private String salt;

    private String email;

    private boolean isActivated;

    private String guid;

    private Date userCreated;


    public User(String username, String hashedPassword, String salt, String email, boolean isActivated, String guid, Date userCreated) {
        this.username = username;
        this.hashedPassword = hashedPassword;
        this.salt = salt;
        this.email = email;
        this.isActivated = isActivated;
        this.guid = guid;
        this.userCreated = userCreated;
    }



}
