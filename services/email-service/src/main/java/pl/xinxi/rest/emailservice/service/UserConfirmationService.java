package pl.xinxi.rest.emailservice.service;

import pl.xinxi.rest.emailservice.bean.User;

public interface UserConfirmationService {

    void sendActivationEmail(User user);
}
