package pl.xinxi.rest.emailservice.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.xinxi.rest.emailservice.bean.EmailSender;
import pl.xinxi.rest.emailservice.bean.User;

import java.math.BigDecimal;
import java.util.Date;
import java.util.logging.Logger;

@RestController
public class EmailController {

    private EmailSender emailSender;

    public EmailController(EmailSender emailSender) {
        this.emailSender = emailSender;
    }

    private Logger logger = Logger.getLogger(EmailController.class.getName());

    @PostMapping("/activateUser")
    public ResponseEntity retrieveExchangeValue(@RequestBody User user){
        logger.info("Generating email for user:" + user);
        emailSender.sendEmail(user);
        return ResponseEntity.ok("STATUS.OK");
    }
}
