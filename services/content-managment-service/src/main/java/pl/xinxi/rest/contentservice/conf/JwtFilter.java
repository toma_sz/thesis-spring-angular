package pl.xinxi.rest.contentservice.conf;

import org.springframework.web.filter.GenericFilterBean;
import pl.xinxi.rest.contentservice.service.feignclients.UserServiceProxy;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.SignatureException;

public class JwtFilter extends GenericFilterBean {

    private UserServiceProxy userServiceProxy;

    public JwtFilter(UserServiceProxy userServiceProxy) {
        this.userServiceProxy = userServiceProxy;
    }

    public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain chain)
            throws IOException, ServletException {

        final HttpServletRequest request = (HttpServletRequest) req;
        final HttpServletResponse response = (HttpServletResponse) res;
        final String authHeader = request.getHeader("authorization");

        if ("OPTIONS".equals(request.getMethod())) {
            response.setStatus(HttpServletResponse.SC_OK);

            chain.doFilter(req, res);
        } else {
            if (authHeader == null || !authHeader.startsWith("Bearer ")) {
                throw new ServletException("Missing or invalid Authorization header");
            }

            final String token = authHeader.substring(7);

            req.setAttribute("userCredentials", userServiceProxy.getUserCredentials(authHeader));


            chain.doFilter(req, res);
        }
    }
}
