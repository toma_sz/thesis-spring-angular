package pl.xinxi.rest.contentservice.service.feignclients;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import pl.xinxi.rest.contentservice.bean.UserCredentials;

@FeignClient(name = "zuul-gateway")
@RibbonClient(name = "accounts-service")
public interface UserServiceProxy {

    @GetMapping("accounts-service/userDetails/authenticateUser")
    UserCredentials getUserCredentials(@RequestHeader("Authorization") String token);
}
