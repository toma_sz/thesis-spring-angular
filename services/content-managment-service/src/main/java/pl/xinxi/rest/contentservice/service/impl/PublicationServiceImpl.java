package pl.xinxi.rest.contentservice.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pl.xinxi.rest.contentservice.bean.Publication;
import pl.xinxi.rest.contentservice.bean.PublicationHttpResponse;
import pl.xinxi.rest.contentservice.bean.UserCredentials;
import pl.xinxi.rest.contentservice.repository.PublicationRepository;
import pl.xinxi.rest.contentservice.service.PublicationService;


import java.nio.file.AccessDeniedException;
import java.util.List;
import java.util.Optional;

@Service
public class PublicationServiceImpl implements PublicationService{

    private PublicationRepository repository;

    public PublicationServiceImpl(PublicationRepository repository) {
        this.repository = repository;
    }

    @Override
    public void savePublication(String title, String description, UserCredentials userCredentials, Optional<Long> publicationId) {
        Publication publication = new Publication(title, description, userCredentials.getUsername());
        if (publicationId.isPresent()){
            publication.setId(publicationId.get());
        }
        repository.save(publication);
    }

    @Override
    public void deletePublication(Long publicationId, UserCredentials userCredentials) throws AccessDeniedException {
        Optional<Publication> publication = repository.findById(publicationId);
        if (publication.isPresent() && publication.get().getUserName().equals(userCredentials.getUsername())){
            repository.deleteById(publicationId);
        }
        else throw new AccessDeniedException("User with id:" + userCredentials.getId() + " dont have permission to publication id:"+ publicationId);
    }

    @Override
    public Page<Publication> getPublications(int limit, int page, Optional<String> searchValue, String orderBy, String orderDirection) {
        Pageable pageable = getPageable(limit,page,orderBy,orderDirection);
        if (searchValue.isPresent()){
            return getPublicationsBySearch(searchValue.get().toLowerCase(),pageable);
        }
        return repository.findAll(pageable);
    }

    private Page<Publication> getPublicationsBySearch(String searchValue, Pageable pageable){

        return repository.getAllByDescriptionIsContainingIgnoreCaseOrTitleContainingIgnoreCaseOrUserNameContainingIgnoreCase(searchValue,searchValue,searchValue, pageable);
    }

    private Pageable getPageable(int limit, int page, String orderBy, String orderDirection){
        Sort.Direction direction = orderDirection.equals("desc") ? Sort.Direction.DESC : Sort.Direction.ASC;
        Sort sort = new Sort(direction, orderBy);
        return PageRequest.of(page, limit, sort);
    }
}
