package pl.xinxi.rest.contentservice.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.xinxi.rest.contentservice.bean.HttpResponse;

@RestController
@RequestMapping("/test")
public class TestController {
    @Value("${server.port}")
    private String serverPort;

    @GetMapping
    public HttpResponse getServerPort(){
        return new HttpResponse(serverPort);
    }
}
