package pl.xinxi.rest.contentservice.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
public class UserCredentials {

    private Long id;
    private String username;
    private String role;

}
