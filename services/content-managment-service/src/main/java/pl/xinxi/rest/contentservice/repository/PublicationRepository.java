package pl.xinxi.rest.contentservice.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import pl.xinxi.rest.contentservice.bean.Publication;


public interface PublicationRepository extends PagingAndSortingRepository<Publication, Long> {


    Page<Publication> getAllByDescriptionIsContainingIgnoreCaseOrTitleContainingIgnoreCaseOrUserNameContainingIgnoreCase(
            String description, String title, String userName, Pageable pageable);

}
