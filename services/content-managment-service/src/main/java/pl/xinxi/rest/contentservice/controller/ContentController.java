package pl.xinxi.rest.contentservice.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.xinxi.rest.contentservice.bean.HttpResponse;
import pl.xinxi.rest.contentservice.bean.Publication;
import pl.xinxi.rest.contentservice.bean.PublicationHttpResponse;
import pl.xinxi.rest.contentservice.bean.UserCredentials;
import pl.xinxi.rest.contentservice.service.PublicationService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/publication")
@CrossOrigin
public class ContentController {

    private PublicationService publicationService;

    public ContentController(PublicationService publicationService) {
        this.publicationService = publicationService;
    }

    @GetMapping("/test")
    public ResponseEntity retrieveExchangeValue(HttpServletRequest request){
        System.out.println(request.getAttribute("userCredentials"));
        return ResponseEntity.ok(new HttpResponse("OK"));
    }

    @PostMapping
    public ResponseEntity<HttpResponse> addPublication(@RequestBody Publication publication, HttpServletRequest request){
        publicationService.savePublication(publication.getTitle(), publication.getDescription(), getUserCredentials(request), Optional.ofNullable(publication.getId()));
        return ResponseEntity.ok(new HttpResponse("OK"));
    }

    @PutMapping
    public ResponseEntity<HttpResponse> editPublication(@RequestBody Publication publication, HttpServletRequest request){
        return addPublication(publication,request);
    }

    @GetMapping
    public ResponseEntity<Page<Publication>> getPublications(@RequestParam("limit") int limit, @RequestParam("page") int page,
                                                             @RequestParam("orderBy") String orderBy, @RequestParam("orderDirection") String orderDirection,
                                                             @RequestParam("searchValue") Optional<String> searchValue){
        return ResponseEntity.ok(publicationService.getPublications(limit,page,searchValue,orderBy,orderDirection));

    }

    @DeleteMapping
    public ResponseEntity delete(@RequestParam("publicationId") Long publicationId, HttpServletRequest request) throws Exception{
        publicationService.deletePublication(publicationId, getUserCredentials(request));
        return ResponseEntity.ok(new HttpResponse("OK"));
    }

    private UserCredentials getUserCredentials(HttpServletRequest request){
        return (UserCredentials) request.getAttribute("userCredentials");
    }

}
