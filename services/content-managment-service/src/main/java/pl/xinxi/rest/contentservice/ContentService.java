package pl.xinxi.rest.contentservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.sleuth.sampler.AlwaysSampler;
import org.springframework.context.annotation.Bean;
import pl.xinxi.rest.contentservice.conf.JwtFilter;
import pl.xinxi.rest.contentservice.service.feignclients.UserServiceProxy;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class ContentService {

	public static void main(String[] args) {
		SpringApplication.run(ContentService.class, args);
	}

	@Autowired
	private UserServiceProxy userServiceProxy;

	@Bean
	public AlwaysSampler defaultSampler() {
		return new AlwaysSampler();
	}

	@Bean
	public FilterRegistrationBean jwtFilter() {
		final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		registrationBean.setFilter(new JwtFilter(userServiceProxy));
		registrationBean.addUrlPatterns("/publication/*");

		return registrationBean;
	}
}
