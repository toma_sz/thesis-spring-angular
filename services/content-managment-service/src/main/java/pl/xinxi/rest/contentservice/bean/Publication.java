package pl.xinxi.rest.contentservice.bean;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@ToString
public class Publication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;
    @Lob
    private String description;
    private Date createdDate;
    private Date lastModifiedDate;
    private String userName;

    public Publication(String title, String description, String userName) {
        this.title = title;
        this.description = description;
        this.userName= userName;
        Date date = new Date();
        this.createdDate = date;
        this.lastModifiedDate = date;
    }
}
