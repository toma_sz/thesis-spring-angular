package pl.xinxi.rest.contentservice.service;

import org.springframework.data.domain.Page;
import pl.xinxi.rest.contentservice.bean.Publication;
import pl.xinxi.rest.contentservice.bean.PublicationHttpResponse;
import pl.xinxi.rest.contentservice.bean.UserCredentials;

import java.nio.file.AccessDeniedException;
import java.util.List;
import java.util.Optional;

public interface PublicationService {

    void savePublication(String title, String description, UserCredentials userCredentials, Optional<Long> publicationId);
    void deletePublication(Long publicationId, UserCredentials userCredentials) throws AccessDeniedException;
    Page<Publication> getPublications(int limit, int page, Optional<String> searchValue, String orderBy, String orderDirection);
}
