package pl.xinxi.rest.contentservice.bean;

import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class PublicationHttpResponse {
    List<Publication> publications;
    Long count;
}
