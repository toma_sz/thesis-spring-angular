#!/bin/bash

function cdToWorkDir {
               cd $(cd -P -- "$(dirname -- "$0")" && pwd -P)
           }

cdToWorkDir
PORT=$1
for f in *; do
    if [[ -d $f ]]; then
        $f/docker-start.sh $PORT 127.0.0.1
        ((PORT++))
    fi
done
