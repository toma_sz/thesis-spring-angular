import { Component, OnInit } from '@angular/core';
import {ApiKeyService} from '../ApiKeyService';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-wrapper',
  templateUrl: './wrapper.component.html',
  styleUrls: ['./wrapper.component.css']
})
export class WrapperComponent implements OnInit {
  constructor(private apiKeyService: ApiKeyService, private route: ActivatedRoute) {
    this.apiKeyService = apiKeyService;
  }

  ngOnInit() {
  }

}
