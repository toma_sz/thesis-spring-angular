
export const apiEndpoint = 'http://localhost:8765/';
export const registerEndpoint = apiEndpoint + 'accounts-service/addUser';
export const loginEndpoint = apiEndpoint + 'accounts-service/login';
export const usernameEndpoint = apiEndpoint + 'accounts-service/userDetails/getUsername';
export const publicationEndpoint = apiEndpoint + 'content-service/publication';
export const activateEndpoint = apiEndpoint + 'accounts-service/activateUser/';
export class User {
  username: string;
  password: string;
  email: string;

  constructor(username: string, password: string, email: string) {
    this.username = username;
    this.password = password;
    this.email = email;
  }
}

export class Publication {
  createdDate: Date;
  description: string;
  id: number;
  lastModifiedDate: Date;
  title: string;
  userName: string;

  constructor(description: string, title: string, id: number) {
      this.createdDate = new Date();
      this.description = description;
      this.lastModifiedDate = new Date();
      this.title = title;
      this.userName = 'username';
      this.id = id;
  }
}

export class ApiKey {
  apiKey: string;
}

export class PublicationResponse {
  content: Publication[];
}
export class Username {
  username:  string;
}
