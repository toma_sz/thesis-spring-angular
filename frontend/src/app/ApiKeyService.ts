import {Injectable, OnInit} from '@angular/core';
import {log} from 'util';

@Injectable()
export class ApiKeyService {

  apiKey: string;
  constructor() {
    this.apiKey = localStorage.getItem('apiKey');
  }
}
