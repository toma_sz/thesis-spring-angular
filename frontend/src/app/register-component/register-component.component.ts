import {Component, OnInit} from '@angular/core';
import {HttpService} from '../HttpService';

@Component({
  selector: 'app-register-component',
  templateUrl: './register-component.component.html',
  styleUrls: ['./register-component.component.css']
})
export class RegisterComponent implements OnInit {
  username: string;
  password: string;
  email: string;
  httpService: HttpService;
  isRegistered: boolean;

  constructor(httpService: HttpService) {
    this.httpService = httpService;
  }

  ngOnInit() {
  }

  onClickRegister() {
    this.httpService.registerUser(this.username, this.password, this.email).subscribe(data=>{
      this.isRegistered = true;
    }, error => {this.isRegistered = false});
  }
}
