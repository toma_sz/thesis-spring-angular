import { Component, OnInit } from '@angular/core';
import {HttpService} from '../HttpService';
import {DataService} from '../DataService';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login-component',
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.css']
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;
  httpService: HttpService;
  dataService: DataService;
  loginError = false;
  constructor(httpService: HttpService, dataService: DataService, private router: Router) {
    this.httpService = httpService;
    this.dataService = dataService;
  }

  ngOnInit() {
  }
  onClickLogin() {
    console.log('login');
    this.httpService.loginUser(this.username, this.password).subscribe(data  => {
      localStorage.setItem('apiKey', data.apiKey);
      this.router.navigateByUrl('/main');
      location.reload();
    }, error => this.loginError = true);
  }
}
