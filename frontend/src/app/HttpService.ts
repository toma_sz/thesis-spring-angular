import {Injectable} from '@angular/core';
import { Http, Response } from '@angular/http';
import {
  activateEndpoint,
  apiEndpoint, ApiKey, loginEndpoint, Publication, publicationEndpoint, PublicationResponse, registerEndpoint,
  User, Username, usernameEndpoint
} from './AppConsts';
import {catchError} from 'rxjs/operators';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {HttpClient, HttpParams} from '@angular/common/http';
import {timeout} from 'rxjs/operator/timeout';
import {ApiParametersService} from '../../DataService';

@Injectable()
export class HttpService {
  constructor(private http: HttpClient, private apiParametersService: ApiParametersService) {
  }
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
  registerUser(username: string, password: string, email: string) {
    this.log(registerEndpoint)
    return this.http.post(registerEndpoint, new User(username, password, email));
  }
  loginUser(username: string, password: string): Observable<ApiKey> {
  return this.http.post<ApiKey>(loginEndpoint, new User(username, password, ''));
  }
  private log(s: string) {
      console.log(s);
    }
  savePublication(title: string, description: string, id: number) {
    return this.http.post(publicationEndpoint, new Publication(description,  title, id));
  }
  deletePublication(id: number) {
    let params = new HttpParams();
    params = params.append('publicationId', id.toString());
    return this.http.delete(publicationEndpoint, {params: params});
  }
  getPublications(): Observable<PublicationResponse> {
    let params = new HttpParams();
    params = params.append('page', this.apiParametersService.offset);
    params = params.append('limit', this.apiParametersService.limit);
    params = params.append('orderBy', this.apiParametersService.orderBy);
    params = params.append('orderDirection', this.apiParametersService.orderDirection);
    return this.http.get<PublicationResponse>(publicationEndpoint, {params: params});
  }
  getUsername(): Observable<Username> {
    return this.http.get<Username>(usernameEndpoint)
  }
  activateUser(guid: String): Observable<any> {
    return this.http.get(activateEndpoint + guid);
  }
}

