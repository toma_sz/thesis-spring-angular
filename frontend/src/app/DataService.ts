import {Injectable} from '@angular/core';

@Injectable()
export class DataService {
  private username: string;
  setUsername(username: string) {
    this.username = username;
  }
  getUsername(): string {
    return this.username;
  }
}
