import { Component, OnInit } from '@angular/core';
import {ApiKeyService} from '../ApiKeyService';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-main-component',
  templateUrl: './main-component.component.html',
  styleUrls: ['./main-component.component.css']
})
export class MainComponentComponent implements OnInit {

  ngOnInit() {
  }

}
