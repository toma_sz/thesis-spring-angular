import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpService} from '../HttpService';

@Component({
  selector: 'app-activate',
  templateUrl: './activate.component.html',
  styleUrls: ['./activate.component.css']
})
export class ActivateComponent implements OnInit, OnDestroy {

  guid: string;
  private sub: any;

  constructor(private route: ActivatedRoute, private httpService: HttpService) {}

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.guid = params['guid']; // (+) converts string 'id' to a number
        this.httpService.activateUser(this.guid).subscribe();
      // In a real app: dispatch action to load the details here.
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
