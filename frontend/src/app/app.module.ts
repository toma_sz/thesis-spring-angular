import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './login-component/login-component.component';
import { RegisterComponent } from './register-component/register-component.component';
import { DataComponent } from './data-component/data-component.component';
import {HttpService} from './HttpService';
import {ApiKeyService} from './ApiKeyService';
import {TokenInterceptor} from './TokenInterceptor';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import { DataItemComponent } from './data-component/data-item/data-item.component';
import {DataService} from './DataService';
import {AppRoutingModule} from './app-routing.module';
import { MainComponentComponent } from './main-component/main-component.component';
import {ApiParametersService} from '../../DataService';
import { WrapperComponent } from './wrapper/wrapper.component';
import { ActivateComponent } from './activate/activate.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    DataComponent,
    DataItemComponent,
    MainComponentComponent,
    WrapperComponent,
    ActivateComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [HttpService, ApiKeyService, DataService, ApiParametersService,{
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
