import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Publication} from '../../AppConsts';
import {DataService} from '../../DataService';

@Component({
  selector: 'app-data-item',
  templateUrl: './data-item.component.html',
  styleUrls: ['./data-item.component.css']
})
export class DataItemComponent implements OnInit {

  @Input() publication: Publication;
  @Input() counter: number;
  @Output() idSelectedToEdit = new EventEmitter<Publication>();
  @Output() idSelectedToDelete = new EventEmitter<number>();
  private dataService: DataService;
  constructor(dataService: DataService) {
    this.dataService = dataService;
  }

  ngOnInit() {
  }
  onEditClick(publication: Publication) {
    this.idSelectedToEdit.emit(this.publication);
  }
  onDeleteClick(id: number) {
    this.idSelectedToDelete.emit(id);
  }
}
