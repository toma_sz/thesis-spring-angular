import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {HttpService} from '../HttpService';
import {Publication} from '../AppConsts';
import {DataService} from '../DataService';
import {ApiParametersService} from '../../../DataService';
import {Router} from '@angular/router';

@Component({
  selector: 'app-data-component',
  templateUrl: './data-component.component.html',
  styleUrls: ['./data-component.component.css']
})
export class DataComponent implements OnInit {
  private titleText: string;
  private descriptionText: string;
  private publicationId: number;
  private publicationList: Publication[];
  private isEditMode = false;
  constructor(private httpService: HttpService, private dataService: DataService,
              private apiParametersService: ApiParametersService, private router: Router) {
  }

  ngOnInit() {
    this.httpService.getPublications().subscribe(data => this.publicationList = data.content);
    this.httpService.getUsername().subscribe(
      data => {this.dataService.setUsername(data.username)},
      error => {localStorage.removeItem('apiKey')});
  }

  onClickAddNewPublication() {
    this.httpService.savePublication(this.titleText, this.descriptionText, this.publicationId).subscribe(data => {
      this.refreshList();
      this.titleText = null;
      this.descriptionText = null;
      this.publicationId = null;
    });
  }

  idChanged(publication: Publication) {
    this.publicationId = publication.id;
    this.titleText = publication.title;
    this.descriptionText = publication.description;
    this.isEditMode = true;
  }
  deletePublication(publicationId: number) {
    this.httpService.deletePublication(publicationId).subscribe(data => {
      this.refreshList();
    });
  }
  limitChange(num: string) {
    this.apiParametersService.limit = num;
  }
  offsetChange(num: string) {
    this.apiParametersService.offset = num;
  }
  orderDirectionChange(dir: string) {
    this.apiParametersService.orderDirection = dir;
  }
  orderByChange(by: string) {
    this.apiParametersService.orderBy = by;
  }
  refreshList() {
    this.httpService.getPublications().subscribe(data => this.publicationList = data.content);
  }
  logout() {
    localStorage.removeItem('apiKey');
    location.reload();
  }
}
