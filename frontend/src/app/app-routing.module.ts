import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login-component/login-component.component';
import {RegisterComponent} from './register-component/register-component.component';
import {DataComponent} from './data-component/data-component.component';
import {DataItemComponent} from './data-component/data-item/data-item.component';
import {WrapperComponent} from './wrapper/wrapper.component';
import {ActivateComponent} from './activate/activate.component';

const appRoutes: Routes = [
  {path: '', redirectTo: '/main', pathMatch: 'full'},
  {path: 'main', component: WrapperComponent},
  {path: 'data', component: DataComponent, children: [
    {path: ':id/details', component: DataItemComponent}
  ]},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
  {path: 'data', component: DataComponent},
  { path: 'activate/:guid', component: ActivateComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
