import {Injectable} from '@angular/core';

@Injectable()
export class ApiParametersService {
  private _orderBy = 'id';
  private _orderDirection = 'asc';
  private _limit = '10';
  private _offset = '0';
  get orderBy(): string {
    return this._orderBy;
  }

  set orderBy(value: string) {
    this._orderBy = value;
  }

  get orderDirection(): string {
    return this._orderDirection;
  }

  set orderDirection(value: string) {
    this._orderDirection = value;
  }

  get limit(): string {
    return this._limit;
  }

  set limit(value: string) {
    this._limit = value;
  }

  get offset(): string {
    return this._offset;
  }

  set offset(value: string) {
    this._offset = value;
  }
}
