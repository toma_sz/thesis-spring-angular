create database if not EXISTS test;
use test;
CREATE TABLE if not exists publication (id bigint NOT NULL AUTO_INCREMENT, created_date datetime, description longtext, last_modified_date datetime, title varchar(255), user_name varchar(255), PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8 DEFAULT COLLATE=utf8_general_ci;
CREATE TABLE if not exists user (user_id bigint NOT NULL AUTO_INCREMENT, email varchar(255), guid varchar(255), hashed_password varchar(255), is_activated bit NOT NULL, role varchar(255), salt varchar(255), user_created datetime, username varchar(255) NOT NULL, PRIMARY KEY (user_id), CONSTRAINT UK_sb8bbouer5wak8vyiiy4pf2bx UNIQUE (username), CONSTRAINT UK_ob8kqyqqgmefl0aco34akdtpe UNIQUE (email)) ENGINE=InnoDB DEFAULT CHARSET=utf8 DEFAULT COLLATE=utf8_general_ci;
